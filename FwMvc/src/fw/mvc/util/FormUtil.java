package fw.mvc.util;

import java.lang.reflect.Field;

import javax.servlet.http.HttpServletRequest;

import fw.mvc.AbstractForm;

public class FormUtil {
	public static AbstractForm createForm(HttpServletRequest req, String formClassName) throws InstantiationException, IllegalAccessException, ClassNotFoundException{
		Class formClass = Class.forName(formClassName);
		AbstractForm form = (AbstractForm)formClass.newInstance();
		
		Field[] fields = formClass.getDeclaredFields();
		for(Field f : fields){
			f.setAccessible(true);
			f.set(form, req.getParameter(f.getName()));
			f.setAccessible(false);
		}
		
		return form;
	}
	
	
}
