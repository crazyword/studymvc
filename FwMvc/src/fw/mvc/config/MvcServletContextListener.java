package fw.mvc.config;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.jdom2.JDOMException;

public class MvcServletContextListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		// TODO Auto-generated method stub
		String appRealPath = sce.getServletContext().getRealPath("/");
		String configPath = appRealPath + "/WEB-INF/struts-config.xml";
		try {
			ConfigMap configMap = new ConfigMap(new File(configPath));
			sce.getServletContext().setAttribute("configMap", configMap);
			System.out.println(configMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
