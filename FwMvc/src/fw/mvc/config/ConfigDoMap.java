package fw.mvc.config;

import java.util.HashMap;

/**
 * key: String(forward condition)
 * value: String(renderer page)
 * @author feng xu on 20140919.
 *
 */
public class ConfigDoMap extends HashMap<String, String> {

	private String path;
	private String formClass;
	private String actionClass;
	
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getFormClass() {
		return formClass;
	}
	public void setFormClass(String formClass) {
		this.formClass = formClass;
	}
	public String getActionClass() {
		return actionClass;
	}
	public void setActionClass(String actionClass) {
		this.actionClass = actionClass;
	}
	
	public String toString(){
		StringBuffer sb = new StringBuffer();
		sb.append("path=").append(path)
			.append("; formClass=").append(formClass)
			.append("; actionClass=").append(actionClass);
		this.forEach((condition, forward) -> 
				sb.append("\n  condition=").append(condition).append("; renderer=").append(forward));
		return sb.toString();
	}
}
