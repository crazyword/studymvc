package fw.mvc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fw.mvc.config.ConfigDoMap;

public abstract class AbstractAction {
	public abstract String execute(
			HttpServletRequest req, 
			HttpServletResponse resp, 
			AbstractForm form, 
			ConfigDoMap configDoMap
			);
}
