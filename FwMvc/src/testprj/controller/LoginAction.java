package testprj.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import testprj.view.LoginForm;
import fw.mvc.AbstractAction;
import fw.mvc.AbstractForm;
import fw.mvc.config.ConfigDoMap;

public class LoginAction extends AbstractAction {

	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp,
			AbstractForm form, ConfigDoMap configDoMap) {
		String result = null; 
		LoginForm loginForm = (LoginForm)form;
		if(loginForm.getUserName().equals(loginForm.getPassword())){
			result = configDoMap.get("success");
		}else{
			result = configDoMap.get("failed");
		}
		
		return result;
	}



}
